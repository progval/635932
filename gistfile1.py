# ...
from supybot.i18n import PluginInternationalization, internationalizeDocstring
_ = PluginInternationalization('Status')

class Status(callbacks.Plugin):
    def __init__(self, irc):
        self.__parent = super(Status, self)
        self.__parent.__init__(irc)
        # XXX It'd be nice if these could be kept in the registry.
        self.sentMsgs = 0
        self.recvdMsgs = 0
        self.sentBytes = 0
        self.recvdBytes = 0
        self.connected = {}

    @internationalizeDocstring
    def net(self, irc, msg, args):
        """takes no arguments

        Returns some interesting network-related statistics.
        """
        print _ # Works
        # ...
    net = wrap(net)

    @internationalizeDocstring
    def cpu(self, irc, msg, args):
        """takes no arguments

        Returns some interesting CPU-related statistics on the bot.
        """
        print _ # Fails
        # ...
    cpu = wrap(cpu)